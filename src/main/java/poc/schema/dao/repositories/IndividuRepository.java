package poc.schema.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import poc.schema.model.millesime.Individu;

import java.util.List;
@Repository
public interface IndividuRepository extends JpaRepository<Individu,Long> {

    List<Individu> findAll();
}
