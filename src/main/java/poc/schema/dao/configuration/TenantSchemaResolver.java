package poc.schema.dao.configuration;


import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;
import poc.schema.model.millesime.SchemaMillesime;
import poc.schema.web.model.TenantContext;

/**
 * CurrentTenantIdentifierResolver:
 * It tells hibernate which is the current configured tenant.
 * It uses the previous ThreadLocal variable set by the interceptor.
 * If no Tenant id is found then the Public schema is used as default tenant
 **/
@Component
public class TenantSchemaResolver implements CurrentTenantIdentifierResolver {

    private String defaultTenant = SchemaMillesime.SCHEMA_2019;

    @Override
    public String resolveCurrentTenantIdentifier() {
        String t =  TenantContext.getCurrentTenant();
        if(t!=null){
            return t;
        } else {
            return defaultTenant;
        }
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}